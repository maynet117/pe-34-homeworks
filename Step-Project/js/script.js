$(document).ready(function() {
    $('.slider').slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        variableWidth: false,
        asNavFor:'.main-slider',
        focusOnSelect: true,
        

    });
    $('.main-slider').slick({
        arrows: false,
        fade: true,
        asNavFor: '.slider',


    });
});



let currentCategory = 'web-design';

const container = document.querySelector('#navigation');


container.addEventListener('click', (event) => {
    currentCategory = event.target.dataset.category;

    const items = document.querySelectorAll('.services-content');
    items.forEach(item => {
        item.classList.remove('visible');

        if (item.dataset.category === currentCategory) {
            item.classList.add('visible');
        }
    })

    const tabs = document.querySelectorAll('li');
    tabs.forEach(item => {
        item.classList.remove('active');

        if (item.dataset.category === currentCategory) {
            item.classList.add('active');
        }
    })

});


const images = [
    {
        src: 'img/graphic-design/graphic-design1.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design2.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design3.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design4.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design5.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design6.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design7.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design8.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design9.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design10.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design11.jpg',
        category: 'graphic'
    }, {
        src: 'img/graphic-design/graphic-design12.jpg',
        category: 'graphic'
    }, {
        src: 'img/web-design/web-design1.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design2.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design3.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design4.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design5.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design6.jpg',
        category: 'web'
    }, {
        src: 'img/web-design/web-design7.jpg',
        category: 'web'
    }, {
        src: 'img/landing-page/landing-page1.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page2.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page3.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page4.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page5.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page6.jpg',
        category: 'landing'
    }, {
        src: 'img/landing-page/landing-page7.jpg',
        category: 'landing'
    }, {
        src: 'img/wordpress/wordpress1.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress2.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress3.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress4.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress5.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress6.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress7.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress8.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress9.jpg',
        category: 'wordpress'
    }, {
        src: 'img/wordpress/wordpress10.jpg',
        category: 'wordpress'
    },
]


let perPage = 12;
let thisCategory = 'all';




const show = (arr) => {
    const mainContainer = document.querySelector('.work-content');
    const sliceArr = arr.slice(0, perPage);
    const htmlArr = sliceArr.map((elem) => {
        return `<img class="work-content-item" src="${elem.src}">`
    });
    mainContainer.innerHTML = htmlArr.join(' ');

    const btn = document.querySelector('.load-more');

    if (perPage >= arr.length) {
        btn.classList.add('hide');
    } else {
        btn.classList.remove('hide');
    };
};
show(images);

const filterArr = (arr, category) => {
    return arr.filter((e) => e.category === category)
};

const itemContainer = document.querySelector('.work-item');

itemContainer.addEventListener('click', (event) => {
    const category = event.target.dataset.category;
    thisCategory = category;
    perPage = 12;

    if (category === 'all' && event.target !== event.currentTarget) {
        images.sort(function() {
            return Math.random() - 0.5;
        })
        .length = images.length;
        show(images);
    } else if (event.target !== event.currentTarget) {
        const newArray = filterArr(images, category);
        show(newArray);
    }

    const itemLink = document.querySelector('.work-link');
    itemLink.forEach(e => {
        e.classList.remove('main');
    });
    event.target.classList.add('main');
});

const button = document.querySelector('.load-more');
button.addEventListener('click', () => {
    perPage = perPage + 12;
    if (thisCategory === 'all') {
        show(images);
    } else {
        show(filterArr(images, thisCategory));
    }
});





