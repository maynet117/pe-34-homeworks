function createNewUser() {
	return {
	firstName: prompt('Enter your name'),
	lastName: prompt('Enter your last name'),
	birthday: prompt('Enter your date of birth, in format dd.mm.yyyy'),
	getLogin: function () {
		return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
	},

	getAge: function () {
		const now = new Date();
		const birthdayDate = this.birthday.split('.');
		const date = new Date(birthdayDate[2], birthdayDate[1] - 1, birthdayDate[0]);
		const time = now - date;
		return new Date(Date.now() - date).getFullYear() - 1970;
	},

	getPassword: function () {
		return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split('.')[2];
	}
}
	
}

const newUser = createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());